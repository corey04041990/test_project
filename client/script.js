window.onload = function () {
    fetch('http://localhost:3000/api/users')
        .then(response => response.json())
        .then(users => {
            const tableBody = document.getElementById('users-table').getElementsByTagName('tbody')[0];

            users.forEach(user => {
                let newRow = tableBody.insertRow();

                let nameCell = newRow.insertCell(0);
                let cityCell = newRow.insertCell(1);
                let imagesCountCell = newRow.insertCell(2);

                nameCell.textContent = user.name;
                cityCell.textContent = user.city;
                imagesCountCell.textContent = user.images_count;
            });
        })
        .catch(error => console.error('Error:', error));
};

document.getElementById('userForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const formData = new FormData();
    formData.append('name', document.getElementById('name').value);
    formData.append('city', document.getElementById('city').value);
    formData.append('image', document.getElementById('image').files[0]);

    fetch('http://localhost:3000/api/users', {
        method: 'POST',
        body: formData
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            alert('User successfully created');
        })
        .catch(error => {
            console.error('Error:', error);
            alert('An error occurred');
        });
});