const Router = require('express');
import multer from 'multer';
const router = new Router();
import userController from '../controllers/users';

const upload = multer({ dest: 'uploads/' });

router.post('/users', upload.single('image'), userController.create);
router.get('/users', userController.getAll);

export default router;