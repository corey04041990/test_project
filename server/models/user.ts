import { Model, DataTypes } from 'sequelize';
import sequelize from '../db';
import UserImage from './userImage';

interface UserAttributes {
    id: number;
    name: string;
    city: string;
}

class User extends Model<UserAttributes> {
    declare id: number;
    declare name: string;
    declare city: string;

}

User.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        city: {
            type: DataTypes.STRING,
        },
    },
    {
        sequelize,
        tableName: 'users',
        createdAt: 'created_at',
        updatedAt: false,
    },
);

User.hasMany(UserImage, { foreignKey: 'user_id' });

export default User;