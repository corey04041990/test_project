import sequelize from '../db';
import { Model, DataTypes } from 'sequelize';

interface UserImageAttributes {
    user_id: number;
    image: string;
}

export default class UserImage extends Model<UserImageAttributes> {
    declare user_id: number;
    declare image: string;
}

UserImage.init({
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    image: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'user_images',
    createdAt: 'created_at',
    updatedAt: false,
});