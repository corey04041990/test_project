import { Request, Response } from 'express';
import User from '../models/user';
import UserRepository, { IFilter } from '../repositories/user';
import UserImage from '../models/userImage';

class UsersController {
    async create(req: Request, res: Response) {
        try {
            const newUser = await User.create(req.body)

            if (req.file) {
                const file = req.file;
                await UserImage.create({
                    user_id: newUser.id,
                    image: `${file.path}.${file.mimetype.split('/')[1]}`,
                });
            }
            res.json(newUser);
        } catch (e) {
            res.status(500).json(e);
        }
    }

    async getAll(req: Request, res: Response) {
        try {
            const users = await UserRepository.getAllUsersSortedByImagesCnt(req.query.filter as IFilter);
            res.json(users);
        } catch (e) {
            res.status(500).json(e);
        }
    }
}

export default new UsersController();