import User from '../models/user';
(async () => {
    const batchSize = 500;
    const totalUsers = 10000;

    try {
        for (let i = 0; i < totalUsers; i += batchSize) {
            let users = Array.from({ length: batchSize }, (_, k) => ({
                name: `example${i + k}`,
                city: `city${i + k}`,
            }));

            await User.bulkCreate(users);
        }
    } catch (e) {
        console.error(e);
    }
})();