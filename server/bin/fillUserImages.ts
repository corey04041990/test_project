import User from '../models/user';
import UserImage from '../models/userImage';

(async () => {
    const batchSize = 500;
    const totalUsers = 100000;

    try {
        const users = await getUsers();

        for (let i = 0; i < totalUsers; i += batchSize) {
            let userImages = Array.from({ length: batchSize }, (_, k) => {
                const user = users[Math.floor(Math.random() * users.length)];

                return {
                    user_id: user.id,
                    image: `image${i + k}`,
                };
            });
            await UserImage.bulkCreate(userImages);
        }
    } catch (e) {
        console.error(e);
    }
})();

async function getUsers(): Promise<User[]> {
    let limit = 500,
        lastChunkLength = 0,
        offset = 0;
    const users = [];

    do {
        const usersChunk = await User.findAll({
            limit,
            offset,
        });

        users.push(...usersChunk);

        lastChunkLength = usersChunk.length;

        offset += limit;
    } while (lastChunkLength > 0);

    return users;
}