import sequelize from '../db';

export interface IFilter {
    page?: number;
}

export default {
    async getAllUsersSortedByImagesCnt(filter?: IFilter) {
        const limit = 20;
        const offset = (filter?.page || 1 - 1) * limit;

        const query = `
            SELECT *, ui.images_count
            FROM users u
                 LEFT JOIN (SELECT user_id, COUNT(*) AS images_count
                            FROM user_images
                            GROUP BY user_id) ui ON u.id = ui.user_id
            WHERE ui.images_count > 0
            ORDER BY ui.images_count DESC
            LIMIT :limit OFFSET :offset
        `;

        try {
            const [results] = await sequelize.query(query, {
                raw: true,
                replacements: {
                    limit,
                    offset,
                },
            });
            return results;
        } catch (error) {
            console.error(error);
        }
    },
};